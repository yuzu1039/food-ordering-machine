import javax.swing.*;
import java.awt.event.*;

public class FoodOrderingMachine {
    private JPanel root;
    private JButton soySauceRamenButton;
    private JButton saltRamenButton;
    private JButton misoRamenButton;
    private JButton chashumenButton;
    private JButton tonkotsuRamenButton;
    private JButton tsukemenButton;
    private JTextPane orderedItemsList;
    private JButton checkOutButton;
    private JLabel totalLabel;

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachine");
        frame.setContentPane(new FoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    int order(String foodName,int price){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+foodName+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation==0){
            JOptionPane.showMessageDialog(null, "Thank you for ordering "+ foodName +"! It will be served as soon as possible.");
            String currentText = orderedItemsList.getText();
            orderedItemsList.setText(currentText + foodName + " " + price + "yen" + "\n");
            totalPrice+=price;
            totalLabel.setText("Total "+totalPrice+"yen");
        }
        return totalPrice;
    }

    int totalPrice=0;
    public FoodOrderingMachine() {
        soySauceRamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Soy-sauce Ramen",600);
                /*
                soySauceRamenButton.setIcon(new ImageIcon(
                        this.getClass().getResource("C:\\Users\\futia\\IdeaProjects\\Report1\\image\\ramen.jpg")
                ));
                */
            }
        });

        saltRamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Salt Ramen",650);
            }
        });
        misoRamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Miso Ramen",700);
            }
        });
        chashumenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Chashu-men",1000);
            }
        });
        tonkotsuRamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tonkotsu Ramen",850);
            }
        });
        tsukemenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tsukemen",900);
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if(confirmation==0){
                    JOptionPane.showMessageDialog(null, "Thank you. The total price is "+totalPrice+"yen");
                    orderedItemsList.setText("");
                    totalPrice=0;
                    totalLabel.setText("Total "+totalPrice+"yen");
                }
            }
        });

    }

}
